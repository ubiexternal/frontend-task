
# GETTING STARTED

1. To get started you will need to install the Angular-cli tool (https://cli.angular.io/) and create a new Angular 5 project;

2. You can add any new npm module that you might need;

3. For style you can use pure CSS, or any of the LESS or SASS pre-processors;


# SPECIFICATIONS

You will have to develop this application based on the following specifications:

1. As the app's user when open the application I want to see:
	* the company's name
	* the company's	description
	* a list with all the departments names

2. When clicking on a department I get redirected to the department's page where I see:
	* department's name
	* department's description
	* all the information about the team leader (including a list of all his/hers skills with their values (see data.structure.js for details))
	* a list of all the members names of the department

3. When clicking on a member's name I get redirected to the member's page where I see:
	* all the information about the member (including a list of all his/hers skills with their values)
	* 2 buttons (initially disabled) SAVE and UNDO
	* a canvas HTML 5 tag, with a chart describing the member's skills, it can be a bar chart, pie chart or any other representation, creativity is always appreciated.  
    * the canvas chart should be drawn natively, without the use of an external chart library 

4. On the member's page I have possibility to change the values of his/hers skills:
    * the SAVE and UNDO buttons will be enabled. And the actual value change will be made only when the SAVE button is clicked
    * the UNDO button will, of course, undo that change
    * the canvas chart will react at the changes and refresh accordingly 
    
5. The data structure regarding the company is defined in [data.structure.js](data.structure.js);

6. The necessary data for this application is found at the following end point (API mock): http://private-ff3a9-companyorganisation.apiary-mock.com ;


# EXTRA POINTS

1. On the department's page I want to see:
    * ADD button, that will add a new member for that department
    * FORM that will open when the ADD button is clicked, containing all fields needed to create a new member (including the possibility of adding multiple skills and their values)
    * SUBMIT button on the FORM that will add the new member, once is clicked (as we have only mock API sent data won't be really saved)

2. On the member's page I want to see:
	* the possibility of adding a skill
	* the possibility of removing a skill
	

# SUGGESTIONS

1. Pay attention not only to functionality but also to usability and nice UI;

2. You can use Bootstrap (or any other style library), but feel free to write your own custom style; 

3. The endpoints are on apiary-mock and, even though PUT and POST requests are allowed you will NOT change the actual data;

4. If, for any reason, you might get stuck, don't waste time, feel free to contact us for suggestions or ideas (we encourage and appreciate team work);